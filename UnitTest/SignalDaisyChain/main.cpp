#include <QCoreApplication>
#include <QtTest>

#include "UnitTestLib_SignalDaisyChain_UnitTest.h"

int main(int argc, char** argv)
{
    QCoreApplication a(argc, argv);

    UnitTestLib_SignalDaisyChain_UnitTest test;

    return QTest::qExec(&test, argc, argv);
}
