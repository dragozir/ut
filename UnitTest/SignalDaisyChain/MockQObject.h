#ifndef MOCKQOBJECT_H
#define MOCKQOBJECT_H

#include <QObject>
#include "Ut/std/helper_classes.h"

class MockQObject : public QObject
{
    Q_OBJECT
public:
    explicit MockQObject(QObject *parent = nullptr);

signals:
    void signal_int1(int);
    void signal_int2(int);
    void signal_int3(int);

    void signal_int_char1(int, char);
    void signal_int_char2(int, char);
    void signal_int_char3(int, char);

    void signal_empty_struct1(Ut::empty);
    void signal_empty_struct2(Ut::empty);
    void signal_empty_struct3(Ut::empty);

    void signal_no_copy_struct1(const Ut::no_copy&);
    void signal_no_copy_struct2(const Ut::no_copy&);
    void signal_no_copy_struct3(const Ut::no_copy&);

//    void signal_no_move_struct1(Ut::no_move);
//    void signal_no_move_struct2(Ut::no_move);
//    void signal_no_move_struct3(Ut::no_move);


public slots:
};

#endif // MOCKQOBJECT_H
