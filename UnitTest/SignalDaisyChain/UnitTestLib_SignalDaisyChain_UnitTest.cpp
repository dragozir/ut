#include "UnitTestLib_SignalDaisyChain_UnitTest.h"

// QtTest
#include <QtTest/QtTest>
#include "Ut/Qt/SignalDaisyChain.h"
#include "Ut/std/helper_classes.h"

UnitTestLib_SignalDaisyChain_UnitTest::UnitTestLib_SignalDaisyChain_UnitTest(QObject *parent) : QObject(parent)
{

}

void UnitTestLib_SignalDaisyChain_UnitTest::init()
{
    m_pMockQObject = new MockQObject();
    qRegisterMetaType<Ut::empty>();
}

void UnitTestLib_SignalDaisyChain_UnitTest::cleanup()
{
    delete m_pMockQObject;
}

void UnitTestLib_SignalDaisyChain_UnitTest::test_signalDaisyChain()
{
    QSignalSpy spy_signal_int1(m_pMockQObject, &MockQObject::signal_int1);
    QSignalSpy spy_signal_int2(m_pMockQObject, &MockQObject::signal_int2);
    QSignalSpy spy_signal_int3(m_pMockQObject, &MockQObject::signal_int3);

    QObject::connect(m_pMockQObject, &MockQObject::signal_int1,
                     m_pMockQObject, &MockQObject::signal_int2);
    QObject::connect(m_pMockQObject, &MockQObject::signal_int2,
                     m_pMockQObject, &MockQObject::signal_int3);

    Ut::test_signal_daisy_chain(m_pMockQObject, &MockQObject::signal_int1,
                                m_pMockQObject, &MockQObject::signal_int2,
                                m_pMockQObject, &MockQObject::signal_int3,
                                9);

    QCOMPARE(spy_signal_int1.count(), 1);
    QCOMPARE(spy_signal_int2.count(), 2);
    QCOMPARE(spy_signal_int3.count(), 3);

    QSignalSpy spy_signal_int_char1(m_pMockQObject, &MockQObject::signal_int_char1);
    QSignalSpy spy_signal_int_char2(m_pMockQObject, &MockQObject::signal_int_char2);
    QSignalSpy spy_signal_int_char3(m_pMockQObject, &MockQObject::signal_int_char3);

    QObject::connect(m_pMockQObject, &MockQObject::signal_int_char1,
                     m_pMockQObject, &MockQObject::signal_int_char2);
    QObject::connect(m_pMockQObject, &MockQObject::signal_int_char2,
                     m_pMockQObject, &MockQObject::signal_int_char3);

    Ut::test_signal_daisy_chain(m_pMockQObject, &MockQObject::signal_int_char1,
                                m_pMockQObject, &MockQObject::signal_int_char2,
                                m_pMockQObject, &MockQObject::signal_int_char3,
                                4, 'c');

    QCOMPARE(spy_signal_int_char1.count(), 1);
    QCOMPARE(spy_signal_int_char2.count(), 2);
    QCOMPARE(spy_signal_int_char3.count(), 3);

    QSignalSpy spy_signal_empty_struct1(m_pMockQObject, &MockQObject::signal_empty_struct1);
    QSignalSpy spy_signal_empty_struct2(m_pMockQObject, &MockQObject::signal_empty_struct2);
    QSignalSpy spy_signal_empty_struct3(m_pMockQObject, &MockQObject::signal_empty_struct3);

    QObject::connect(m_pMockQObject, &MockQObject::signal_empty_struct1,
                     m_pMockQObject, &MockQObject::signal_empty_struct2);
    QObject::connect(m_pMockQObject, &MockQObject::signal_empty_struct2,
                     m_pMockQObject, &MockQObject::signal_empty_struct3);

    Ut::test_signal_daisy_chain(m_pMockQObject, &MockQObject::signal_empty_struct1,
                                m_pMockQObject, &MockQObject::signal_empty_struct2,
                                m_pMockQObject, &MockQObject::signal_empty_struct3,
                                Ut::empty{});

    QCOMPARE(spy_signal_empty_struct1.count(), 1);
    QCOMPARE(spy_signal_empty_struct2.count(), 2);
    QCOMPARE(spy_signal_empty_struct3.count(), 3);

}


