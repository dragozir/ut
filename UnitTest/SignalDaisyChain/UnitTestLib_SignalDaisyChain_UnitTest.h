#pragma once
// Qt
#include <QObject>
// mock object
#include "MockQObject.h"

class UnitTestLib_SignalDaisyChain_UnitTest : public QObject
{
    Q_OBJECT
public:
    explicit UnitTestLib_SignalDaisyChain_UnitTest(QObject *parent = nullptr);

signals:

private slots:
    void init();
    void cleanup();
    
//    void test_signalDaisyChain_data();
    void test_signalDaisyChain();
    
private:
    MockQObject* m_pMockQObject;

};
