#include <QCoreApplication>
#include <QtTest>

#include "UnitTestLib_ConnectionExists_UnitTest.h"

int main(int argc, char** argv)
{
    QCoreApplication a(argc, argv);

    UnitTestLib_ConnectionExists_UnitTest test;

    return QTest::qExec(&test, argc, argv);
}
