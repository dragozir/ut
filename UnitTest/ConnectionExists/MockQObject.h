#ifndef MOCKQOBJECT_H
#define MOCKQOBJECT_H

#include <QObject>

class Empty{};

class MockQObject : public QObject
{
    Q_OBJECT
public:
    explicit MockQObject(QObject *parent = nullptr);

signals:
    void signal_int1(int);
    void signal_int2(int);
    void signal_int3(int);

    void signal_int_char1(int, char);
    void signal_int_char2(int, char);
    void signal_int_char3(int, char);

    void signal_empty_struct1(Empty);
    void signal_empty_struct2(Empty);
    void signal_empty_struct3(Empty);


public slots:
};

#endif // MOCKQOBJECT_H
