#pragma once
// Qt
#include <QObject>
// mock object
#include "MockQObject.h"

class UnitTestLib_ConnectionExists_UnitTest : public QObject
{
    Q_OBJECT
public:
    explicit UnitTestLib_ConnectionExists_UnitTest(QObject *parent = nullptr);

signals:

private slots:
    void init();
    void cleanup();
    
//    void test_signalDaisyChain_data();
    void test_connectionExists();
    
private:
    MockQObject* m_pMockQObject;

};
