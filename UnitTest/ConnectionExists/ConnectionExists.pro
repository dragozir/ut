QT += core
QT += testlib

TARGET = UnitTestLib_ConnectionExists_UnitTest

CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

BUILD_DIR = unittest

DESTDIR = build/$$BUILD_DIR/bin
MOC_DIR = build/$$BUILD_DIR/moc
OBJECTS_DIR = build/$$BUILD_DIR/obj
UI_DIR = build/$$BUILD_DIR/ui
RCC_DIR = build/$$BUILD_DIR/rcc

QMAKE_CXXFLAGS -= -Wall
QMAKE_CXXFLAGS += -Wno-unused-but-set-variable -Wno-unused-variable
QMAKE_CXXFLAGS += -Wno-unused-parameter
QMAKE_CXXFLAGS += -DRTI_UNIX
QMAKE_CXXFLAGS += -DRTI_LINUX
QMAKE_CXXFLAGS += -DRTI_64BIT
QMAKE_CXXFLAGS += -m64
QMAKE_CXXFLAGS += --coverage
QMAKE_LFLAGS   += --coverage

CXX_FLAGS += -ggdb -fexceptions

INCLUDEPATH += . \
               ../../Ut/include

HEADERS += \
    $$files($$PWD/*.h,false) \

SOURCES += \
    $$files($$PWD/*.cpp,false) \
