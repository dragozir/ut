#include "UnitTestLib_ConnectionExists_UnitTest.h"

// QtTest
#include <QtTest/QtTest>
#include "Ut/Qt/ConnectionExists.h"

UnitTestLib_ConnectionExists_UnitTest::UnitTestLib_ConnectionExists_UnitTest(QObject *parent) : QObject(parent)
{

}

void UnitTestLib_ConnectionExists_UnitTest::init()
{
    m_pMockQObject = new MockQObject();
}

void UnitTestLib_ConnectionExists_UnitTest::cleanup()
{
    delete m_pMockQObject;
}

void UnitTestLib_ConnectionExists_UnitTest::test_connectionExists()
{
    QObject::connect(m_pMockQObject, &MockQObject::signal_int1,
                     m_pMockQObject, &MockQObject::signal_int2);
    QObject::connect(m_pMockQObject, &MockQObject::signal_int2,
                     m_pMockQObject, &MockQObject::signal_int3);

    Ut::test_connection(m_pMockQObject, &MockQObject::signal_int1,
                        m_pMockQObject, &MockQObject::signal_int2);

    Ut::test_connection(m_pMockQObject, &MockQObject::signal_int2,
                        m_pMockQObject, &MockQObject::signal_int3);

    Ut::test_connection(m_pMockQObject, &MockQObject::signal_int1,
                        m_pMockQObject, &MockQObject::signal_int2,
                        m_pMockQObject, &MockQObject::signal_int3);

    Ut::test_connection(m_pMockQObject,
                        &MockQObject::signal_int1,
                        &MockQObject::signal_int2,
                        &MockQObject::signal_int3);

    Ut::test_connection(m_pMockQObject,
                        &MockQObject::signal_int1,
                        &MockQObject::signal_int2,
                        m_pMockQObject, &MockQObject::signal_int3);

    QEXPECT_FAIL("", "\n\
                 Testing connection between non-connected signals:\n\
                 &MockQObject::signal_int1 & &MockQObject::signal_int3\n\
                 XFAIL expected",
                 Continue);

    Ut::test_connection(m_pMockQObject, &MockQObject::signal_int1,
                        m_pMockQObject, &MockQObject::signal_int3);

    QEXPECT_FAIL("", "\n\
                 Testing connection after disconnecting signals:\n\
                 &MockQObject::signal_int1 & &MockQObject::signal_int2\n\
                 XFAIL expected",
                 Continue);

    QObject::disconnect(m_pMockQObject, &MockQObject::signal_int1,
                        m_pMockQObject, &MockQObject::signal_int2);

    Ut::test_connection(m_pMockQObject,
                        &MockQObject::signal_int1,
                        &MockQObject::signal_int2,
                        &MockQObject::signal_int3);

    QWARN("Testing ConnectionBehvaior::Warning:\n\
          Disconnecting all signals, expecting 2 warnings.");

    QObject::disconnect(m_pMockQObject, &MockQObject::signal_int2,
                        m_pMockQObject, &MockQObject::signal_int3);

    constexpr auto flag = Ut::ConnectionBehavior::Warn;

    Ut::test_connection<flag>(m_pMockQObject,
                              &MockQObject::signal_int1,
                              &MockQObject::signal_int2,
                              &MockQObject::signal_int3);

    qDebug() << m_pMockQObject->metaObject()->className();
}


