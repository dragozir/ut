#-------------------------------------------------
#
# Project created by Jake Hyde 2018-08-09T5:10:32
#
#-------------------------------------------------
QT  += gui core xml xmlpatterns testlib

TEMPLATE = lib
CONFIG += debug

U_ROOT = $$PWD/ \

HEADERS += $$files($$PWD/include/Ut/*.h, false) \
           $$files($$PWD/include/Ut/Qt/*.h, false) \
           $$files($$PWD/include/Ut/std/*.h, false) \

DEFINES += "U_DEBUG"

QMAKE_CXXFLAGS += -Wall -Werror -Wextra -pedantic -std=c++11

INCLUDEPATH  += ./include \
                ./include/Ut \
                ./include/Ut/std \
                ./include/Ut/Qt \
