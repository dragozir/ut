#pragma once

#if defined (U_DEBUG) || defined (QT_DEBUG)
#include <QDebug>
#endif
// Qt
#include <QObject>
#include <QtCore/qobjectdefs.h>

namespace Ut
{
Q_NAMESPACE

namespace detail
{
Q_NAMESPACE

} // namespace detail

struct empty{};

struct no_copy
{
    no_copy() = default;
    no_copy(const no_copy&) = delete;
};

struct no_move
{
    no_move(no_move&&) = delete;
};

struct singleton
{
private:
    singleton();

public:
    inline singleton* instance()
    {
        static singleton* s;
        return s;
    }
};

inline bool operator==(const Ut::empty&, const Ut::empty&)
{
    return true;
}

} // namespace Ut

Q_DECLARE_METATYPE(Ut::empty)


//bool operator==(const Ut::no_copy&, const Ut::no_copy&)
//{
//    return true;
//}

