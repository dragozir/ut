#pragma once

// Qt
#include <QtCore/qobjectdefs.h>

// std
#include <type_traits>

namespace Ut
{
Q_NAMESPACE

// Member Function Wrapper
template <typename T, typename R, typename... Fargs>
using member_function = R(T::*)(Fargs...);

// using type alias for a succint version of the typedef Qt implements in QObject::connect
template <typename T, typename... Fargs>
using qt_metafunction = member_function<T, void, Fargs...>;
//using qt_metafunction = typename QtPrivate::FunctionPointer<void (T::*)(Fargs...)>::Function;


// Note: If two objects have operator==, they can be used with QCOMPARE macro.
// Otherwise they call overloaded template function for comparing, which each class must implement.
// Then, in test can check that overload exists, and fail static assert with warning to include header for your type.

#define DECL_HAS_OPERATOR(name,operator_type)\
    struct has_operator_##name##_h{};\
    \
    template <typename T, typename U>\
    has_operator_##name##_h& operator operator_type (const T&, const U&);\
    \
    template <typename T, typename U = T>\
    struct has_operator_##name\
    {\
        constexpr static bool value = !std::is_same<decltype(std::declval<T&>() operator_type std::declval<U&>()), has_operator_##name##_h&>::value;\
    }

DECL_HAS_OPERATOR(equals, ==);
DECL_HAS_OPERATOR(plus, +);
DECL_HAS_OPERATOR(minus, -);
DECL_HAS_OPERATOR(left_shift, <<);
DECL_HAS_OPERATOR(right_shift, >>);

}
