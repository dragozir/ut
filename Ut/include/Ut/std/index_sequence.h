#pragma once

#if defined (U_DEBUG) || defined (QT_DEBUG)
#include <QDebug>
#endif

namespace Ut
{
Q_NAMESPACE

// allows for creation of a parameter pack of type std::size_t from 0 to N-1 eg. {0, 1, 2, ..., N-1}
// Sample Usage:
// function call: typename make_indexes<N>::type{}
// function parameter: (indexes<I...>, ...)
// in calling function: std::get<I>(tuple)...
// expands to std::get<0>(tuple), std::get<1>(tuple), ..., std::get<N-1>(tuple).
// can be used to initialize a container, in a runtime or constexpr context if using all LiteralTypes

template <std::size_t... I>
struct indexes
{
    using type = indexes;
};

template <std::size_t N, std::size_t... I>
struct make_indexes
{
    using type_aux = typename std::conditional<
    (N == sizeof...(I)),
    indexes<I...>,
    make_indexes<N, I..., sizeof...(I)>>::type;
    using type = typename type_aux::type;
};

} // namespace Ut
