#pragma once

#if defined (U_DEBUG) || defined (QT_DEBUG)
#include <QDebug>
#endif

// QtTest
#include <QtTest/QtTest>
// lib
#include "Ut/std/type_traits_t.h"

namespace Ut
{
Q_NAMESPACE

template <typename T, typename U = T, typename F>
inline void test_pureGetSet(T* t,
                            member_function<T, void, F> set,
                            member_function<U, F> get,
                            F f)
{
    QCOMPARE(f, (t->*set(t,f), (t->*get(t)))); // comma operator
}

} // namespace Ut
