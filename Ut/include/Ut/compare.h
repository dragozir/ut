#pragma once

#if defined (U_DEBUG) || defined(QT_DEBUG)
#include <QDebug>
#endif

// QtTest
#include <QtTest/QtTest>

// lib
#include "Ut/std/type_traits_t.h"

namespace Ut
{
Q_NAMESPACE

enum class ComparisonFlag
{
    CompareAllFields,
    CountDifferences
};
Q_DECLARE_FLAGS(ComparisonFlags, ComparisonFlag)

namespace detail
{
Q_NAMESPACE

// Function Prototype which must be implemented in each class.

template <typename T, typename U>
inline void countDifferences(const T& t, const U& u, size_t diff = 0);

// undefref all instantiations need to be specialized regardless of default enum
template <ComparisonFlag f>
struct CompareWrapper;

template <>
struct CompareWrapper<ComparisonFlag::CompareAllFields>{
    template <typename T, typename U>
    static void compare_helper(const T& t, const U& u)
    {
#ifdef U_DEBUG
        qDebug() << Q_FUNC_INFO;
#endif
        QCOMPARE(t, u);
    }

    template <typename T, typename U, typename... Args>
    static void compare_helper(const T& t, const U& u, Args... args)
    {
#ifdef U_DEBUG
        qDebug() << Q_FUNC_INFO;
#endif
        QCOMPARE(t, u);

        CompareWrapper<ComparisonFlag::CompareAllFields>::compare_helper(u, args...);
    }
};

template <>
struct CompareWrapper<ComparisonFlag::CountDifferences>
{
    template <typename T, typename U>
    static inline void compare_helper(const T& t, const U& u, std::size_t sz)
    {
#ifdef U_DEBUG
        qDebug() << Q_FUNC_INFO;
#endif
        detail::countDifferences(t, u, sz);
    }

    template <typename T, typename U, typename... Args>
    static inline void compare_helper(const T& t, const U& u, std::size_t sz, Args... args)
    {
#ifdef U_DEBUG
        qDebug() << Q_FUNC_INFO;
#endif
        detail::countDifferences(t, u, sz);

        CompareWrapper<ComparisonFlag::CountDifferences>::compare_helper(u, args...);
    }

};

template <typename T, typename U>
inline void difference(const T& t, const U& u, size_t& diff)
{
    QCOMPARE(t, u);
    if (t != u)
    {
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO << "t=" << t << ", u=" << u << ", diff=" << diff;
#endif
        diff++;
    }
}

} // namespace detail

template <ComparisonFlag f = ComparisonFlag::CompareAllFields, typename T, typename U, typename... Args>
inline void compare(const T& lhs, const U& rhs, Args... args)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    detail::CompareWrapper<f>::compare_helper(lhs, rhs, args...);
}

}// namespace UNIT_TEST
