#pragma once

// std
#include <array>
// Qt
#include <QtCore/qobjectdefs.h>
#include <QString>
// QtTest
#include <QtTest/QtTest>

namespace Ut
{
Q_NAMESPACE

namespace detail
{
Q_NAMESPACE

template <typename Ts>
struct array_data;

template <>
struct array_data<bool>
{
    constexpr static const std::array<bool, 2> arr =
    {
        {
            true,
            false
        }
    };
};
// C++17 defines all constexpr static members as implictly inline
#if __cplusplus < 201703L
constexpr const std::array<bool, 2> array_data<bool>::arr;
#endif

template<>
struct array_data<std::ptrdiff_t>
{
    constexpr static const std::array<std::ptrdiff_t, 7> arr =
    {
        {
            0,
            1,
            -1,

            INT8_MAX,
            INT16_MAX,
            INT32_MAX,
            INT64_MAX
        }
    };
};

#if __cplusplus < 201703L
constexpr const std::array<std::ptrdiff_t, 7> array_data<std::ptrdiff_t>::arr;
#endif

template<>
struct array_data<std::size_t>
{
    constexpr static const std::array<std::size_t, 6> arr =
    {
        {
            0,
            1,

            UINT8_MAX,
            UINT16_MAX,
            UINT32_MAX,
            UINT64_MAX
        }
    };
};

#if __cplusplus < 201703L
constexpr const std::array<std::size_t, 6> array_data<std::size_t>::arr;
#endif

} // namespace detail

template <typename T, std::size_t sz, std::ptrdiff_t upper_bound, std::ptrdiff_t lower_bound>
struct generate_random_numbers
{
    static std::array<T, sz> arr;

    template <typename U = T>
    static typename std::enable_if<std::is_integral<U>::value, const std::array<T,sz>&>::type
    fillArray()
    {
        std::random_device rand_dev;
        std::mt19937 rng(rand_dev());

        auto id = std::uniform_int_distribution<U>(lower_bound, upper_bound);
        for (auto i = 0; i < sz; i++)
        {
            arr[i] = id(rng);
        }
        return arr;
    }

    template <typename U = T>
    static typename std::enable_if<std::is_floating_point<U>::value, const std::array<T,sz>&>::type
    fillArray()
    {
        std::random_device rand_dev;
        std::mt19937 rng(rand_dev());

        auto id = std::uniform_real_distribution<U>(lower_bound, upper_bound);
        for (auto i = 0; i < sz; i++)
        {
            arr[i] = id(rng);
        }
        return arr;
    }
};

#if __cplusplus < 201703L
template <typename T, std::size_t sz, std::ptrdiff_t upper_bound, std::ptrdiff_t lower_bound>
std::array<T,sz> Ut::generate_random_numbers<T, sz, upper_bound, lower_bound>::arr;
#endif

} // namespace UNIT_TEST
