#pragma once
// DEbug stuff

//template <typename... Fargs>
//struct wrapper
//{
//    template <typename T, typename U, typename R>
//    static inline void test_signalDaisyChain(Fargs... fargs,
//                                             T* t, mem_fun<T, R, Fargs...> ts,
//                                             U* u, mem_fun<U, R, Fargs...> us)
//    {
//        test_connectionExists(t, ts, u, us);

//        QSignalSpy tSpy(t, ts);
//        QSignalSpy uSpy(u, us);

//        (t->*ts)(fargs...);

//        QCOMPARE(tSpy.count(), uSpy.count());
//    }

//    template <typename T, typename U, typename R, typename... Sargs>
//    static inline void test_signalDaisyChain(Fargs... fargs,
//                                             T* t, mem_fun<T, R, Fargs...> ts,
//                                             U* u, mem_fun<U, R, Fargs...> us,
//                                             Sargs... sargs)
//    {
//        qDebug() << "this";
//        test_connectionExists(t, ts, u, us);

//        QSignalSpy tSpy(t, ts);
//        QSignalSpy uSpy(u, us);

//        (t->*ts)(fargs...);
//qDebug() << "t";
//        for (auto it : tSpy)
//        {
//            for (auto i : it)
//            {
//                qDebug() << it;
//            }
//        }
//qDebug() << "u";
//        for (auto it : uSpy)
//        {
//            for (auto i : it)
//            {
//                qDebug() << it;
//            }
//        }

//        QCOMPARE(tSpy.count(), uSpy.count());

//        test_signalDaisyChain(fargs..., u, us, sargs...);
//    }

//};

//template <typename T>
//void helper(T t1, T t2)
//{   qDebug() << t1 << ", " << t2;
//    QCOMPARE(t1 == t2, true);
//}

//template<std::size_t size, typename A, typename T, typename... Args>
//void method(const QSignalSpy& spy, T t)
//{

//    auto msg = qvariant_cast<T>(spy.at(0).at(size-1));
////    auto cMsg = qvariant_cast<T>(cSpy.at(0).at(size-1));

//    helper(msg, t);
////    helper(cMsg, t)
//}

//template<std::size_t size, typename... Args, typename T, typename... Targs>
//void method(const QSignalSpy& spy, const Args&... args, T& t, Targs&... targs)
//{

//    auto msg = qvariant_cast<T>(spy.at(0).at(size-1));
////    auto cMsg = qvariant_cast<T>(cSpy.at(0).at(size-1));

//    helper(msg, t);
////    helper(cMsg, t)
//    method<size>(args..., t, targs...);
//}

//template <std::size_t size, typename T, typename... Args>
//void method(QSignalSpy& sSpy, QSignalSpy& cSpy, T t, Args... args)
//{
//    auto newsize = sizeof...(Args);

//    auto sMsg = qvariant_cast<T>(sSpy.at(0).at(size-newsize-1)); // recurse from 0
//    auto cMsg = qvariant_cast<T>(cSpy.at(0).at(size-newsize-1));

//    helper(sMsg, cMsg);
//    method<size>(sSpy, cSpy, t, args...);
//}

//template <typename T, typename U, typename R, typename... Args>
//static inline void test_signalDaisyChain(T* t, R(T::*ts)(Args...),
//                                         U* u, R(U::*us)(Args...),
//                                         Args... args)
//{
//    test_connectionExists(t, ts, u, us);

//    const QSignalSpy& sSpy = QSignalSpy(t, ts);
//    const QSignalSpy& cSpy = QSignalSpy(u, us);

//    (t->*ts)(args...);

//    QCOMPARE(sSpy.count(), cSpy.count());

//    method<sizeof...(Args), QSignalSpy, Args...>(sSpy, cSpy, args...);
//}

//template <typename T>
//struct tag_t {using type=T;};

//template <typename Tag>
//using type_t = typename Tag::type;

//template <typename T>
//using no_deduction = type_t<tag_t<T>>;


//template <typename T, typename U, typename... Sargs, typename... Fargs>
//void test_signalDaisyChain(T* t, void(T::*t_signal)(Fargs...),
//                           U* u, void(U::*u_signal)(Fargs...),
//                           Sargs... sargs,
//                           Fargs... fargs)
//{
//    static_assert(std::is_base_of<QObject, T>::value, "Error, T must inherit QObject");
//    static_assert(std::is_base_of<QObject, U>::value, "Error, U must inherit QObject");
//}



//#ifdef U_DEBUG
//#include <QDebug>
//#endif

//// Qt
//#include <QObject>
//#include <QMetaObject>

//// QtTest
//#include <QtTest/QtTest>

//namespace Ut
//{
//Q_NAMESPACE

//// Forward Declarations for mixed Qt4/5 testing.
//// It is possible that explicitly casting a signal in a long chain of testing can be too verbose,
////

//template <typename T, typename U, typename... Fargs, typename R = void>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, void (U::*us)(Fargs...));

//template <typename T, typename U, typename... Fargs, typename... Args>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, void (U::*us)(Fargs...),
//                                  Args... args);

//namespace detail
//{
//Q_NAMESPACE

//enum class QtFunctor
//{
//    Signal,
//    Slot
//};

//template<typename T, typename... Args>
//inline QtFunctor qtFunctorToMacro(void (T::*functor)())
//{

//}

//template <typename T, typename U, typename... Fargs, typename R = void>
//inline void test_connectionExists(T* t, void (T::*ts)(Fargs...),
//                                  U* u, void (U::*us)(Fargs...))
//{
//#ifdef U_DEBUG
//    qDebug() << "TypeT:\t" << typeid(t).name() << "\n"
//             << "SIGNAL:\t" << QMetaMethod::fromSignal(ts).name() << "\n"
//             << "TypeU:\t" << typeid(u).name() << "\n"
//             << "SIGNAL:\t" << QMetaMethod::fromSignal(us).name() << "\n";
//#endif
//    QCOMPARE(!(QObject::connect(t, ts, u, us, Qt::UniqueConnection)), true);
//}

//template <typename T, typename U, typename R = void>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, const char* us)
//{
//#ifdef U_DEBUG
//    qDebug() << "TypeT:\t" << typeid(t).name() << "\n"
//             << "SIGNAL:\t" << ts << "\n"
//             << "TypeU:\t" << typeid(u).name() << "\n"
//             << "SIGNAL:\t" << us << "\n";
//#endif
//    QCOMPARE(!(QObject::connect(t, ts, u, us, Qt::UniqueConnection)), true);
//}

//} // namespace detail

//template <typename T, typename U, typename... Fargs, typename R = void>
//inline void test_connectionExists(T* t, void (T::*ts)(Fargs...),
//                                  U* u, void (U::*us)(Fargs...))
//{
//#ifdef U_DEBUG
//    qDebug() << Q_FUNC_INFO;
//#endif
//    detail::test_connectionExists(t, ts, u, us);
//}

//template <typename T, typename U, typename... Fargs, typename... Args>
//inline void test_connectionExists(T* t, void (T::*ts)(Fargs...),
//                                  U* u, void (U::*us)(Fargs...),
//                                  Args... args)
//{
//    qDebug() << "ptr ptr";
//    detail::test_connectionExists(t, ts, u, us);
//    test_connectionExists(u, us, args...);
//}

//template <typename T, typename U, typename R = void>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, const char* us)
//{
//    qDebug() << "string string base";
//    detail::test_connectionExists(t, ts, u, us);
//}

//template <typename T, typename U, typename... Args>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, const char* us,
//                                  Args... args)
//{
//    qDebug() << "string string";
//    detail::test_connectionExists(t, ts, u, us);
//    test_connectionExists(u, us, args...);
//}

//template <typename T, typename U, typename... Fargs, typename R = void>
//inline void test_connectionExists(T* t, void (T::*ts)(Fargs...),
//                                  U* u, const char* us)
//{
//    qDebug() << "ptr stringbase";
//    // The string representation of a Signal starts with 2. Here, we take the benefit that
//    detail::test_connectionExists(t, QString("2" + QMetaMethod::fromSignal(ts).methodSignature()).toStdString().c_str(), u, us);
//}

//template <typename T, typename U, typename... Fargs, typename... Args>
//inline void test_connectionExists(T* t, void (T::*ts)(Fargs...),
//                                  U* u, const char* us,
//                                  Args... args)
//{
//    qDebug() << "ptr string";
//    // The string representation of a Signal starts with 2. Here, we take the benefit that
//    detail::test_connectionExists(t, QString("2" + QMetaMethod::fromSignal(ts).methodSignature()).toStdString().c_str(), u, us);
//    test_connectionExists(u, us, args...);
//}

//template <typename T, typename U, typename... Fargs, typename R>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, void (U::*us)(Fargs...))
//{
//    qDebug() << "string ptrbase";
//    // The string representation of a Signal starts with 2. Here, we take the benefit that
//    auto i = QMetaMethod::fromSignal(us).methodType();
//    //static_assert(i == QMetaMethod::Signal || i == QMetaMethod::Slot, "Please Send a signal or slot to tha function");
//    auto b = (i == QMetaMethod::Signal ? "2" : "1");
//    detail::test_connectionExists(t, ts, u, QString(b + QMetaMethod::fromSignal(us).methodSignature()).toStdString().c_str());
//}

//template <typename T, typename U, typename... Fargs, typename... Args>
//inline void test_connectionExists(T* t, const char* ts,
//                                  U* u, void (U::*us)(Fargs...),
//                                  Args... args)
//{
//    qDebug() << "string ptr";
//    // The string representation of a Signal starts with 2. Here, we take the benefit that
//    auto i = QMetaMethod::fromSignal(us).methodType();
//    //static_assert(i == QMetaMethod::Signal || i == QMetaMethod::Slot, "Please Send a signal or slot to tha function");
//    auto b = (i == QMetaMethod::Signal ? "2" : "1");
//    detail::test_connectionExists(t, ts, u, QString(b + QMetaMethod::fromSignal(us).methodSignature()).toStdString().c_str());
//    test_connectionExists(u, us, args...);
//}


////template <typename... Args>
////struct Types{};

////template <typename... Args>
////struct wrapper2{};

////template <
////        template <typename...> class Types,
////        typename... T1,
////        typename... T2
////        >
////struct wrapper2<Types<T1...>, Types<T2...>>
////{
////    template <typename... T>
////    static inline void consume(T... t)
////    {
////        qDebug() << Q_FUNC_INFO;
////    }
////    static void doSomething(T1... t1, T2... t2)
////    {
////        consume(t1..., t2...);
////    }
////};

//}

//template <std::size_t... I>
//struct indexes
//{
//    using type = indexes;
//};

//template <std::size_t N, std::size_t... I>
//struct make_indexes
//{
//    using type_aux = typename std::conditional<
//    (N == sizeof...(I)),
//    indexes<I...>,
//    make_indexes<N, I..., sizeof...(I)>>::type;
//    using type = typename type_aux::type;
//};

//template <std::size_t sz>
//struct myWrapper
//{
//    template <typename Tuple>
//    static void method(const QSignalSpy& spy, Tuple& tup)
//    {
//        auto t1 = std::get<sz-1>(tup);

//        using type_t = decltype (t1);

//        auto t2 = qvariant_cast<type_t>(spy.at(0).at(sz-1));

//        Ut::compare(t1, t2);

//        myWrapper<sz-1>::method(spy, tup);
//    }
//};

//template <>
//struct myWrapper<0>
//{
//    template <typename Tuple>
//    static void method(const QSignalSpy& spy, Tuple& tup)
//    {
//        auto t1 = std::get<0>(tup);

//        using type_t = decltype (t1);

//        auto t2 = qvariant_cast<type_t>(spy.at(0).at(0));

//        Ut::compare(t1, t2);

//    }
//};

//template <typename Tuple, typename T, typename Method, std::size_t... I>
//void apply_method_impl(Method t_signal, T* t, const Tuple& tup, indexes<I...>)
//{
//    QSignalSpy spy(t, t_signal);

//    (t->*t_signal)(std::get<I>(tup)...);

//    myWrapper<sizeof...(I)>::method(spy, tup);

//    return;
//}

//template <typename Tuple, typename T, typename Method>
//void apply_method(const Tuple& tup, T* t, Method t_signal)
//{
//    apply_method_impl(t_signal, t, tup, typename make_indexes<std::tuple_size<Tuple>::value>::type{});
//}

//template <typename... Fargs, typename... Sargs>
//typename std::enable_if<(sizeof...(Fargs) == sizeof...(Sargs)), void>::type
//test_signal_daisy_chain_impl(const std::tuple<Fargs...>&, Sargs&&...){}

//template <typename... Fargs, typename T, typename... Sargs>
//void test_signal_daisy_chain_impl(const std::tuple<Fargs...>& fargs, T* t, void(T::*t_signal)(Fargs...), Sargs&&... sargs)
//{
//    apply_method(fargs, t, t_signal);

//    test_signal_daisy_chain_impl(fargs, std::forward<Sargs>(sargs)...);
//}

//template <std::size_t I, typename Ret, typename T, typename... Qargs>
//typename std::enable_if<sizeof...(Qargs)+1 == I, Ret>::type
//get_last_n(T&& t, Qargs&&... qargs)
//{
//    return std::make_tuple(std::forward<T>(t), std::forward<Qargs>(qargs)...);
//}

//template <std::size_t I, typename Ret, typename T, typename... Qargs>
//typename std::enable_if<sizeof...(Qargs)+1 != I, Ret>::type
//get_last_n(T&&, Qargs&&... qargs)
//{
//    static_assert(I <= sizeof...(Qargs)+1, "Not enough params");

//    return get_last_n<I, Ret>(std::forward<Qargs>(qargs)...);
//}

//template <typename T, typename... Fargs, typename... Qargs>
//void test_signal_daisy_chain(T* t, void (T::*t_signal)(Fargs...), Qargs&&... qargs)
//{
//    static_assert((sizeof... (Qargs) - sizeof... (Fargs)) % 2 == 0,
//                  "ERROR: Expecting even number of QObject & signals pairs!");

//    auto fargs = get_last_n<sizeof...(Fargs), std::tuple<Fargs...>>(std::forward<Qargs>(qargs)...);

//    test_signal_daisy_chain_impl(fargs, t, t_signal, std::forward<Qargs>(qargs)...);
//}
