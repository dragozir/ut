#pragma once

#if defined (U_DEBUG) || defined (QT_DEBUG)
#include <QDebug>
#endif
// Qt
#include <QObject>
#include <QtCore/qobjectdefs.h>
// QtTest
#include <QtTest/QSignalSpy>
#include <QtTest/QtTest>
// std
#include <type_traits>
#include <tuple>
#include <utility>
// lib
#include "Ut/compare.h"
#include "Ut/Qt/ConnectionExists.h"
#include "Ut/std/type_traits_t.h"
#include "Ut/std/index_sequence.h"

namespace Ut
{
Q_NAMESPACE

enum class DaisyChainBehavior
{
    TestConnection
};
Q_DECLARE_FLAGS(DaisyChainBehaviors, DaisyChainBehavior)

namespace detail
{
Q_NAMESPACE

template <std::size_t sz>
struct myWrapper
{
    template <typename Tuple>
    static void method(const QSignalSpy& spy, Tuple& tup)
    {
        auto t1 = std::get<sz-1>(tup);

        using type_t = decltype (t1);

        auto t2 = qvariant_cast<type_t>(spy.at(0).at(sz-1));

        Ut::compare(t1, t2);

        myWrapper<sz-1>::method(spy, tup);
    }
};

template <>
struct myWrapper<0> // it wrap
{
    template <typename Tuple>
    static void method(const QSignalSpy& spy, Tuple& tup)
    {
        auto t1 = std::get<0>(tup);

        using type_t = decltype (t1);

        auto t2 = qvariant_cast<type_t>(spy.at(0).at(0));

        Ut::compare(t1, t2);

    }
};

template <typename Tuple, typename T, typename Method, std::size_t... I>
void apply_method_impl(Method t_signal, T* t, const Tuple& tup, indexes<I...>)
{
    QSignalSpy spy(t, t_signal);

    (t->*t_signal)(std::get<I>(tup)...);

    myWrapper<sizeof...(I)>::method(spy, tup);

    return; // don't touch
}

template <typename Tuple, typename T, typename Method>
void apply_method(const Tuple& tup, T* t, Method t_signal)
{
    apply_method_impl(t_signal, t, tup, typename make_indexes<std::tuple_size<Tuple>::value>::type{});
}

template <DaisyChainBehavior b, typename... Fargs, typename T, typename... Sargs>
void test_signal_daisy_chain_impl(const std::tuple<Fargs...>& fargs,
                                  T* t, qt_metafunction<T, Fargs...> t_signal,
                                  Sargs&&...){
    apply_method(fargs, t, t_signal);
}

template <DaisyChainBehavior behavior, typename... Fargs, typename T, typename U, typename... Sargs>
void test_signal_daisy_chain_impl(const std::tuple<Fargs...>& fargs,
                                  T* t, qt_metafunction<T, Fargs...> t_signal,
                                  U* u, qt_metafunction<U, Fargs...> u_signal,
                                  Sargs&&... sargs)
{
    switch (behavior)
    {
    case DaisyChainBehavior::TestConnection :
        Ut::test_connection(t, t_signal, u, u_signal);
    }

    apply_method(fargs, t, t_signal);

    test_signal_daisy_chain_impl<behavior>(fargs, std::forward<U*>(u), std::forward<qt_metafunction<U, Fargs...>>(u_signal), std::forward<Sargs>(sargs)...);
}

template <std::size_t I, typename Ret, typename T, typename... Qargs>
typename std::enable_if<sizeof...(Qargs)+1 == I, Ret>::type
get_last_n(T&& t, Qargs&&... qargs)
{
    return std::make_tuple(std::forward<T>(t), std::forward<Qargs>(qargs)...);
}

template <std::size_t I, typename Ret, typename T, typename... Qargs>
typename std::enable_if<sizeof...(Qargs)+1 != I, Ret>::type
get_last_n(T&&, Qargs&&... qargs)
{
    static_assert(I <= sizeof...(Qargs)+1,
                  "ERROR: Not enough parameters applied to test_signal_daisy_chain. Could not create tuple from arguments.");

    return get_last_n<I, Ret>(std::forward<Qargs>(qargs)...);
}

} // namespace detail

template <DaisyChainBehavior b = DaisyChainBehavior::TestConnection,
          typename T, typename... Fargs, typename... Qargs>
void test_signal_daisy_chain(T* t, qt_metafunction<T, Fargs...> t_signal,
                             Qargs&&... qargs)
{

    static_assert((sizeof... (Qargs) - sizeof... (Fargs)) % 2 == 0,
                  "ERROR: Either too many parameters or un-parseable QObject and Signal pairs");

    auto fargs = detail::get_last_n<sizeof...(Fargs), std::tuple<Fargs...>>(std::forward<Qargs>(qargs)...);

    Ut::detail::test_signal_daisy_chain_impl<b>(fargs, t, t_signal, std::forward<Qargs>(qargs)...);
}

} // namespace UNIT_TEST
