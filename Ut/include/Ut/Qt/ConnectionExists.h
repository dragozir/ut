#pragma once

#if defined (U_DEBUG) || defined (QT_DEBUG)
#include <QDebug>
#endif

// Qt
#include <QObject>
#include <QMetaObject>

// QtTest
#include <QtTest/QtTest>

// lib
#include "Ut/std/type_traits_t.h"

namespace Ut
{
Q_NAMESPACE

enum class ConnectionBehavior
{
    Warn,
    Failure
};
Q_DECLARE_FLAGS(ConnectionBehaviors, ConnectionBehavior)

enum class ConnectionStatus : bool
{
    Fail,
    Pass
};
Q_DECLARE_FLAGS(ConnectionStatuses, ConnectionStatus)

/**
 * @brief toString
 * @details - Required for the QTest::qCompare (QCOMPARE macro). Test data now prints
 * "Actual   (connection_exists): Fail
 *  Expected (Connection::Pass) : Pass"
 * which is more apparent to the user.
 * @param t
 * @return
 */
char* toString(const ConnectionStatus& t)
{
    return QTest::toString(t == ConnectionStatus::Fail ? "Fail" : "Pass");
}

namespace detail
{
Q_NAMESPACE

template <ConnectionBehavior behavior, typename T, typename U, typename... Fargs>
typename std::enable_if<behavior == ConnectionBehavior::Warn, void>::type
test_connection_impl(T* const t, qt_metafunction<T, Fargs...> ts,
                     U* const u, qt_metafunction<U, Fargs...> us)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
#ifdef U_EXTRA
    auto t_addr = QString("0x%1").arg(reinterpret_cast<quintptr>(t), QT_POINTER_SIZE * 2, 12, QChar('0'));
    auto u_addr = QString("0x%1").arg(reinterpret_cast<quintptr>(u), QT_POINTER_SIZE * 2, 12, QChar('0'));
    qDebug() << "\n"
             << "TypeT:\t" << t->metaObject()->className() << ": " << t_addr.toStdString().c_str() << "\n"
             << "SIGNAL:\t" << QMetaMethod::fromSignal(ts).name() << "\n"
             << "TypeU:\t" << u->metaObject()->className() << ": " << u_addr.toStdString().c_str() << "\n"
             << "SIGNAL:\t" << QMetaMethod::fromSignal(us).name() << "\n";
#endif
    auto connection_exists = static_cast<ConnectionStatus>(!(QObject::connect(t, ts, u, us, Qt::UniqueConnection)));

    if (connection_exists == ConnectionStatus::Fail)
    {
        QWARN("Connection Failed:");
    }
}

template <ConnectionBehavior behavior, typename T, typename U, typename... Fargs>
typename std::enable_if<behavior == ConnectionBehavior::Failure, void>::type
test_connection_impl(T* const t, qt_metafunction<T, Fargs...> ts,
                     U* const u, qt_metafunction<U, Fargs...> us)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
#ifdef U_EXTRA
    auto t_addr = QString("0x%1").arg(reinterpret_cast<quintptr>(t), QT_POINTER_SIZE * 2, 12, QChar('0'));
    auto u_addr = QString("0x%1").arg(reinterpret_cast<quintptr>(u), QT_POINTER_SIZE * 2, 12, QChar('0'));
    qDebug() << "\n"
             << "TypeT:\t" << t->metaObject()->className() << ": " << t_addr.toStdString().c_str() << "\n"
             << "SIGNAL:\t" << QMetaMethod::fromSignal(ts).name() << "\n"
             << "TypeU:\t" << u->metaObject()->className() << ": " << u_addr.toStdString().c_str() << "\n"
             << "SIGNAL:\t" << QMetaMethod::fromSignal(us).name() << "\n";
#endif
    auto connection_exists = static_cast<ConnectionStatus>(!(QObject::connect(t, ts, u, us, Qt::UniqueConnection)));

    QCOMPARE(connection_exists, ConnectionStatus::Pass);
}

// Prototype
template <ConnectionBehavior b, typename T, typename U, typename... Fargs, typename R = void>
inline void test_connection_delegate(T* const t, qt_metafunction<T, Fargs...> ts,
                                     U* const u, qt_metafunction<U, Fargs...> us);

template <ConnectionBehavior b, typename T, typename... Fargs, typename R = void>
inline void test_connection_delegate(T* const t, qt_metafunction<T, Fargs...> ts, qt_metafunction<T, Fargs...> us)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    detail::test_connection_impl<b>(t, ts, t, us);
}

template <ConnectionBehavior b, typename T, typename... Fargs, typename... Args>
inline void test_connection_delegate(T* const t,
                                     qt_metafunction<T, Fargs...> ts,
                                     qt_metafunction<T, Fargs...> us,
                                     Args... args)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    detail::test_connection_impl<b>(t, ts, t, us);
    test_connection_delegate<b>(t, us, args...);
}

template <ConnectionBehavior b, typename T, typename U, typename... Fargs, typename R>
inline void test_connection_delegate(T* const t, qt_metafunction<T, Fargs...> ts,
                                     U* const u, qt_metafunction<U, Fargs...> us)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    detail::test_connection_impl<b>(t, ts, u, us);
}

template <ConnectionBehavior b, typename T, typename U, typename... Fargs, typename... Args>
inline void test_connection_delegate(T* const t, qt_metafunction<T, Fargs...> ts,
                                     U* const u, qt_metafunction<U, Fargs...> us,
                                     Args... args)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    detail::test_connection_impl<b>(t, ts, u, us);
    test_connection_delegate<b>(u, us, args...);
}

} // namespace detail

template <ConnectionBehavior b = ConnectionBehavior::Failure, typename... Args>
inline void test_connection(Args&&... args)
{
#ifdef U_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    detail::test_connection_delegate<b>(std::forward<Args>(args)...);
}

} // namespace UNIT_TEST
