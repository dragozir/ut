#pragma once

// Qt
#include <QString>
// QtTest
#include <QtTest>

namespace Ut
{

namespace detail
{

template <typename T>
char* toString(const T& t);

} // namespace detail

template <typename T>
char* toString(const T& t)
{
    return detail::toString(t);
}

} // namespace UNIT_TEST
