#pragma once

#if defined (U_DEBUG) || defined (QT_DEBUG)
#include <QDebug>
#endif

// Qt
#include <QObject>
#include <QtCore/qobjectdefs.h>

// std
#include <tuple>
#include <type_traits>
#include <utility>

// lib
#include "Ut/std/index_sequence.h"

namespace Ut
{
Q_NAMESPACE

namespace detail
{
Q_NAMESPACE

// Loop through the it array until it matches the sizes array, resulting in all possible combinations
template <std::size_t N>
bool increase(const std::array<std::size_t, N>& sizes, std::array<std::size_t, N>& it)
{
    for (std::size_t i = 0; i != N; ++i)
    {
        const std::size_t index = N - 1 - i;
        ++it[index];
        if (it[index] >= sizes[index])
        {
            it[index] = 0;
        }
        else
        {
            return true;
        }
    }
    return false;
}

// Unpack a tuple into the callable that we pass.
template <typename F, std::size_t... Is, std::size_t N, typename Tuple>
void exec_cartesian_product_impl(F&& f,
                                 indexes<Is...>,
                                 const std::array<std::size_t, N>& it,
                                 const Tuple& tuple)
{
    //
    f(std::get<Is>(tuple)[it[Is]]...);
}

} // namespace detail

// F = callable argument to be passed to exec
// Ts... = parameter pack of STL compatible containers

template <typename F, typename... Ts>
void exec_cartesian_product(F&& f, Ts&&... vs)
{
    constexpr std::size_t N = sizeof...(Ts);
    std::array<std::size_t, N> sizes{{static_cast<std::size_t>(vs.size())...}};
    // comma operator to create an array filled with zeroes of the same size as our parameter pack (i.e. take size, discard result)
    std::array<std::size_t, N> it{{(vs.size(), 0u)...}}; // 200 iq

    do {
        Ut::detail::exec_cartesian_product_impl(f, typename Ut::make_indexes<N>::type{}, it, std::tie(vs...));
    } while (Ut::detail::increase(sizes, it));
}

} // namespace Ut
