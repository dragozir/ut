TEMPLATE = subdirs

SUBDIRS = \
        Ut

contains(CONFIG, "unittest") {
    SUBDIRS += UnitTest
    UnitTest.depends = Ut
}

